LIB_OBJECTS = error.o
CC = gcc
CFLAGS = -g -O0 -Wall -Wextra

stdin2exec: stdin2exec.o error.o
	$(CC) $(CFLAGS) -o stdin2exec.out stdin2exec.o $(LIB_OBJECTS)

stdin2exec.o: src/stdin2exec.c
	$(CC) $(CFLAGS) -c src/stdin2exec.c

stdin2stdout: stdin2stdout.o error.o
	$(CC) $(CFLAGS) -o stdin2stdout.out stdin2stdout.o $(LIB_OBJECTS)

stdin2stdout.o: src/stdin2stdout.c
	$(CC) $(CFLAGS) -c src/stdin2stdout.c

ls: ls.o error.o
	$(CC) $(CFLAGS) -o ls.out ls.o $(LIB_OBJECTS)

ls.o: src/ls.c
	$(CC) $(CFLAGS) -c src/ls.c

error.o: src/error.c
	$(CC) $(CFLAGS) -c src/error.c

clean:
	$(RM) *.o *.out
